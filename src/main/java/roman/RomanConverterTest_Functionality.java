package roman;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.Ignore;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;

public class RomanConverterTest_Functionality {
	@Test(expected = IllegalArgumentException.class)
	public void test1() throws Exception {
		RomanConverter rc4 = new RomanConverter();
		rc4.toRoman(1);
	}

	@Test
	public void test2() {
		int b = 3988;
		RomanConverter rc2 = new RomanConverter();
		assertEquals(rc2.toRoman(b), "MMMCMLXXXVIII");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test3() throws Exception {
		RomanConverter rc4 = new RomanConverter();
		rc4.toRoman(3999);
	}

	@Test(expected = IllegalArgumentException.class)
	public void test4() throws Exception {
		RomanConverter rc4 = new RomanConverter();
		rc4.toRoman(4000);
	}

	@Test(expected = IllegalArgumentException.class)
	public void test5() throws Exception {
		RomanConverter rc4 = new RomanConverter();
		rc4.toRoman(0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void test6() throws Exception {
		RomanConverter rc4 = new RomanConverter();
		rc4.toRoman(-1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void test7() throws Exception {
		RomanConverter rc4 = new RomanConverter();
		rc4.toRoman(2.5);
	}

	@Test
	public void test8() {
		String a = "MMDXLIX";
		RomanConverter rc1 = new RomanConverter();
		assertEquals(rc1.fromRoman(a), 2549);
	}

	@Test(expected = IllegalArgumentException.class)
	public void test9() throws Exception {
		RomanConverter rc5 = new RomanConverter();
		rc5.fromRoman("ABC");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test10() throws Exception {
		RomanConverter rc5 = new RomanConverter();
		rc5.fromRoman("mmm");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test11() throws Exception {
		RomanConverter rc5 = new RomanConverter();
		rc5.fromRoman("MmL");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test12() throws Exception {
		RomanConverter rc5 = new RomanConverter();
		rc5.fromRoman("TF%");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test13() throws Exception {
		RomanConverter rc5 = new RomanConverter();
		rc5.fromRoman("MM M");
	}
}
