package roman;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.Ignore;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;

@RunWith(Parameterized.class)
public class RomanConverterTest {

	RomanConverter romanConverter;
	Integer number;
	String numeral;

	// initializes roman converter class
	@Before
	public void init() {
		this.romanConverter = new RomanConverter();
	}

	// pass parameters to the tester
	public RomanConverterTest(Integer number, String numeral) {
		this.number = number;
		this.numeral = numeral;
	}

	@Parameters
	public static Collection conversions() {
		return Arrays.asList(new Object[][] { { -1, "" }, { 1, "I" }, { 0, "" } });
	}

	// This test will run with defined parameters
	@Test
	public void testToRoman() {
		System.out.println("Integer being converted : " + number);
		try {
			assertEquals(numeral, romanConverter.toRoman(number));
			System.out.println("Numeral : " + numeral);
		} catch (Exception e) {
			System.out.println("Integer is out of bounds : " + number);
			// System.out.println(e);
		}
		System.out.println("---");
	}


}
