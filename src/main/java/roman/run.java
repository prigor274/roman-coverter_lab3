package roman;

import static org.junit.Assert.assertEquals;

import org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class run {

	public static void main(String[] args) {

		//runs interface tests for toRoman
		Result result = JUnitCore.runClasses(RomanConverterTest.class);
		for (Failure failure : result.getFailures()) {
			System.out.println(failure.toString());
		}
		System.out.println("Interface toRoman Tests success : "+result.wasSuccessful());
		
		//runs interface tests for fromRoman
		Result result2 = JUnitCore.runClasses(RomanConverterTest2.class);
		for (Failure failure : result2.getFailures()) {
			System.out.println(failure.toString());
		}
		System.out.println("Interface toRoman Tests success : "+result2.wasSuccessful());
		
		//runs funtionality based tests
		Result result3 = JUnitCore.runClasses(RomanConverterTest_Functionality.class);
		for (Failure failure : result2.getFailures()) {
			System.out.println(failure.toString());
		}
		System.out.println("Functionality Tests success : "+result2.wasSuccessful());
	}

}
