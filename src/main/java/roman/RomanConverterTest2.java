package roman;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.Ignore;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;

@RunWith(Parameterized.class)
public class RomanConverterTest2 {

	RomanConverter romanConverter;
	int number;
	String numeral;

	// initializes roman converter class
	@Before
	public void init() {
		this.romanConverter = new RomanConverter();
	}

	// pass parameters to the tester
	public RomanConverterTest2(Integer number, String numeral) {
		this.number = number;
		this.numeral = numeral;
	}

	@Parameters
	public static Collection conversions() {
		return Arrays.asList(new Object[][] { { 1, "I" }, { 0, "XoK" },{ 0, "iii" }, {0,""}, {0, "tRL"}, {0, "Tf%"}, {0,"Na N"}});
	}
	

	// This test will run with defined parameters
	@Test
	public void testFromRoman() {
		System.out.println("Numeral being converted : " + numeral);
		try {
			assertEquals(number, romanConverter.fromRoman(numeral));
			System.out.println("Number : " + number);
		} catch (Exception e) {
			System.out.println("Invalid Numeral : " + numeral);
			assertEquals(0, number);
			// System.out.println(e);
		}
		System.out.println("---");
	}

}
